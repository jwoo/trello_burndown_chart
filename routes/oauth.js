"use strict";

var OAuth = require('oauth').OAuth;
var url = require('url');
var querystring = require('querystring');
var request = require('request');
var fs = require('fs');

var requestURL = "https://trello.com/1/OAuthGetRequestToken";
var accessURL = "https://trello.com/1/OAuthGetAccessToken";
var authorizeURL = "https://trello.com/1/OAuthAuthorizeToken";

var trelloConfig = GLOBAL.settings.Trello;
var key = trelloConfig.key;
var secret = trelloConfig.secret;
var loginCallback = trelloConfig.loginCallback;
var appName = trelloConfig.app_name;
var host_name = GLOBAL.settings.host;


var oauth_secrets = {}; //store oauth secret
var oauth = new OAuth(requestURL, accessURL, key, secret, "1.0", loginCallback, "HMAC-SHA1");

function trelloOAuth(req, res, next) {
  oauth.getOAuthRequestToken(function (err, token, tokenSecret, results) {
    if (err) {
      return next(err);
    }

    oauth_secrets[token] = tokenSecret;
    var redirect_url = authorizeURL + '?' + querystring.stringify({oauth_token : token, name : appName});

    res.redirect(redirect_url);
  });
}

function resultCallback(req, res, next) {
  var query = url.parse(req.url, true).query;
  var token = query.oauth_token;
  var tokenSecret = oauth_secrets[token];
  var verifier = query.oauth_verifier;
  
  oauth.getOAuthAccessToken(token, tokenSecret, verifier, function (err, accessToken, TokenSecret, results) {
    if (err) {
      return next(err);
    }
  
    // session에 token이랑 secret을 넣어둡시다.
    oauth.getProtectedResource("https://api.trello.com/1/members/me", "GET", accessToken, TokenSecret, function (error, data, resp) {
      if (error) {
        return next(error);
      }
  
      var user_id = data.id;
      if (!req.session) {
        req.session = {};
      }

      req.session.trello = {
        key : key,
        token : accessToken,
        secret : TokenSecret
      };


      setSession(req.session.trello);
      res.redirect('/board');
    });

  });
}

var setSession = function (session_info) {
  var auth;

  if (typeof session_info === 'object') {
    auth = JSON.stringify(session_info);
  } else {
    auth = session_info;
  }

  fs.writeFileSync('./store.hul', auth);

};

function getSession(req, res, next) {
  fs.readFile('./store.hul', 'ascii', function (err, data) {
    if (err) {
      return next(err);
    }
    
    var auth_info;

    if (typeof data === 'string') {
      auth_info = JSON.parse(data);
    } else {
      auth_info = data;
    }

    if (!req.session) {
      req.session = {};
    }

    req.session.trello = auth_info;
    next();
  });
}

function get_board(req, res, next) {
  request({
    method : "GET",
    url : "https://trello.com/1/members/my/boards",
    qs : {
      key : key,
      token : req.session.trello.token
    },
  }, function (err, resp, body) {
    if (err) {
      return next(err);
    }

    var t_arr = JSON.parse(body);
    var id_arr = [];
    var name_arr = [];

    for (var t in t_arr) {
      id_arr[t] = t_arr[t].id;
      name_arr[t] = t_arr[t].name;
    }

    res.render('index', {
      locals : {
        host : "http://127.0.0.1",
        names : name_arr,
        ids : id_arr
      }
    });

  });
}



module.exports.trelloOAuth = trelloOAuth;
module.exports.resultCallback = resultCallback;
module.exports.get_board = get_board;
module.exports.getSession = getSession;

"use strict";

var request = require('request');
var step = require('step');
var fs = require('fs');

var total_path = '';
var current_path = '';
var trello = require('../model/trello.js');
var store = require('../model/store.js');
var handler = require('../handler');
var resultHandler = handler.resultHandler;

function get_trello_list(req, res, next) {
  var key = req.session.trello.key;
  var token = req.session.trello.token;
  var board_id = req.param('board_id', null);
  var todo_id;
  var doing_id;
  var done_id;

  step(function () {
    trello.getList(board_id, key, token, this);
  },
  function (err, data) {
    if (err) {
      return next(err);
    }
    
    var lists = data.lists;
    trello.listIdExtractor(lists, this);
  },
  function (err, data) {
    if (!data.todo_id) {
      return next(4041);
    } 
    if (!data.doing_id) {
      return next(4042);
    }
    if (!data.done_id) {
      return next(4043);
    }
    
    todo_id = data.todo_id;
    doing_id = data.doing_id;
    done_id = data.done_id;

    resultHandler(res, {
      todo_id : todo_id,
      doing_id : doing_id,
      done_id : done_id
    });

  }
      );
}

function get_trello_card_id_bundle(req, res, next) {
  var key = req.session.trello.key;
  var token = req.session.trello.token;
  var list_id = req.param('list_id', null);

  trello.getCardIdBundle(list_id, key, token, function (err, data) {
    if (err) {
      return next(err);
    }

    resultHandler(res, data);
  });
}

function get_trello_card(req, res, next) {
  var key = req.session.trello.key;
  var token = req.session.trello.token;
  var card_id = req.param('card_id', null);

  //TODO card정보  받아와서 숫자 부분만 저장.
  //숫자, 사진, 제목, 액티비티 내용을 일마다 저장. 
  
}

function get_board_total_count(req, res, next) {
  var key = req.session.trello.key;
  var token = req.session.trello.token;
  var board_id = req.param('board_id', null);

  console.log('hihihihi', token);
  trello.getTotalCount(board_id, key, token, function (err, data) {
    if (err) {
      return next(err);
    }
    
    return next();
  });
  
}

function get_board_current_count(req, res, next) {
  var key = req.session.trello.key;
  var token = req.session.trello.token;
  var todo_id = req.param('todo_id', null);
  var doing_id = req.param('doing_id', null);

  var current_count = 0;

  step(function () {
    trello.getTodoCount(todo_id, key, token, this);
  }, function (err, data) {
    if (err) {
      return next(err);
    }

    current_count += parseInt(data.todo_count, 10);
    trello.getDoingCount(doing_id, key, token, this);
  }, function (err, data) {
    if (err) {
      return next(err);
    }

    current_count += parseInt(data.doing_count, 10);

    store.setCurrentCount(current_count, function (err) {
      if (err) {
        return next(err);
      }
      
      return next();
    });
  }
  );
}

function get_trello_burndown_chart(req, res, next) {
  var key = req.session.trello.key;
  var token = req.session.trello.token;
  var board_id = req.param('board_id', null);
  var todo_id = req.param('todo_id', null);
  var doing_id = req.param('doing_id', null);
  var done_id = req.param('done_id', null);
  var total_count = 0;
  var current_count;

  step(function () {
    trello.getDoneCount(done_id, key, token, this);
  },
  function (err, data) {
    if (err) {
      return next(err);
    }

    total_count += data.done_count;

    trello.getRestCount(board_id, todo_id, doing_id, key, token, this);
  },
  function (err, data) {
    if (err) {
      return next(err);
    }

    var graph_count = data.continue_count;
    var start_date = '2013-02-01';

    total_count += data.today_count;
    graph_count.unshift([start_date, total_count]);
    
    resultHandler(res, {
      total_count : total_count,
      today_count : data.today_count,
      day_count : graph_count,
      start_date : start_date,
      due_date : '2013-03-01'
    });
  }
      );
}

module.exports.get_trello_list = get_trello_list;
module.exports.get_trello_card_id_bundle = get_trello_card_id_bundle;
module.exports.get_trello_burndown_chart = get_trello_burndown_chart;

"use strict";

var Oauth = require('./oauth.js');
var getSession = Oauth.getSession;
var trello = require('./trello.js');

module.exports.routes = function (app) {
  app.get('/', Oauth.trelloOAuth);
  app.get('/auth/result_trello', Oauth.resultCallback);
  app.get('/board', getSession, Oauth.get_board);
  app.get('/board/list', getSession, trello.get_trello_list);
  app.get('/board/burndown', getSession, trello.get_trello_burndown_chart);

};


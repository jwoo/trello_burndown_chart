"use strict";

var request = require('request');
var step = require('step');
var fs = require('fs');

var store = require('./store.js');
var total_path = './total.txt';

//card나 list의 제목에서 남은 카운트를 뽑아낸다.
function countExtractor(name) {
  //name은 내용 (숫자 - 숫자 = 숫자) || 내용 (숫자)
  var count;
  var formula; // (숫자와 -, =만 있음)
  formula = name.replace(/\s+/g, '').replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, '').replace(/[a-zA-Z0-9]*\(/, '('); // 공백 제거

  if (formula.match(/\([0-9]*\-[0-9]*\=[0-9]*\)/)) { // case 1. 이름(숫자-숫자=숫자)
    count = formula.replace(/\([0-9]*\-[0-9]*\=/, '').replace(/\)/, '');
  } else if (formula.match(/\([0-9]+\)/)) { // case 2. 이름(숫자)
    count = formula.replace(/\(/, '').replace(/\)/, '');
  } else {
    count = 0; //숫자가 없는 제목도 있음.
  }

  return parseInt(count, 10);
}

function listIdExtractor(lists, callback) {
  var todo_id;
  var doing_id;
  var done_id;

  for (var i = 0, li = lists.length; i < li; i++) {
    var t = lists[i].name.replace(' ', '');

    if (t.toUpperCase().match('TODO') !== null) {
      todo_id = lists[i].id;
    }
    if (t.toUpperCase().match('DOING') !== null) {
      doing_id = lists[i].id;
    }
    if (t.toUpperCase().match('DONE') !== null) {
      done_id = lists[i].id;
    }
  }

  return callback(null, {
    todo_id : todo_id,
    doing_id : doing_id,
    done_id : done_id
  });  
}

function setListName(list_id, name, key, token, callback) {
  request({
    method : "PUT",
    url : "https://api.trello.com/1/list/" + list_id + "/name",
    json : {
      name : name
    }
  },
  function (err, res, body) {
    if (err) {
      console.log('Set listName Fail');
      return callback(err);
    }

    return callback(null);
  });

}

function getBoard(appKey, token, callback) {
  request({
    method : "GET",
    url : "https://api.trello.com/1/members/my/boards",
    qs : {
      key : appKey,
      token : token
    }
  },
  function (err, res, body) {
    if (err) {
      return callback(err);
    }

    callback(null, body);
  }
  );
}


function getList(board_id, key, token, callback) {
  if (!key) {
    return callback("ERROR : not Key");
  }
  if (!token) {
    return callback("ERROR : not token");
  }
  if (!board_id) {
    return callback("ERROR : not board_id");
  }

  step(
    function () {
      request({
        method : "GET",
        url : "https://api.trello.com/1/boards/" + board_id,
        qs : {
          key : key,
          token : token,
          lists : 'open'
        }
      }, this);
    }, function (err, resp, body) {
      if (err) {
        return callback(err);
      }
      
      var data = JSON.parse(body);

      callback(null, {
        lists : data.lists
      }); 
    }
    );
}


function getCard(card_id, key, token, callback) {
  if (!key) {
    return callback("ERROR : not Key");
  }
  if (!token) {
    return callback("ERROR : not token");
  }
  if (!card_id) {
    return callback("ERROR : not card_id");
  }

  request({
    method : "GET",
    url : "https://api.trello.com/1/cards/" + card_id,
    qs : {
      key : key,
      token : token,
      fields : "open",
      member_fields : "open"
    }
  }, function (err, res, body) {
    if (err) {
      return callback(err);
    }

    var data = JSON.parse(body);

    callback(null, data);
  });
}

//list에 있는 card들의 내용을 갖고온다. card id, 제목을 우선 갖고온다.
function getCardBundle(list_id, key, token, callback) {
  if (!key) {
    return callback("ERROR : not Key");
  }
  if (!token) {
    return callback("ERROR : not token");
  }
  if (!list_id) {
    return callback("ERROR : not list_id");
  }

  request({
    method : "GET",
    url : "https://api.trello.com/1/lists/" + list_id,
    qs : {
      key : key,
      token : token,
      fields : "name",
      cards : "open"
    }
  }, function (err, res, body) {
    if (err) {
      return callback(err);
    }

    var data = JSON.parse(body);

    callback(null, data);
  });
}


//ToDo count를 갖고온다.
//ToDo List의 제목의 카운트와 To Do List내 카드 카운트들의 합을 비교하며 카드 카운트 합이 저장 우선순위가 높다.
function getTodoCount(board_id, todo_id, key, token, callback) {
  if (!key) {
    return callback("ERROR : not Key");
  }
  if (!token) {
    return callback("ERROR : not token");
  }
  if (!board_id) {
    return callback("ERROR : not list_id");
  }
  
  var todoListCount;
  var todoCardSum = 0;
  var modifyName;
 
  step(function () {
    getCardBundle(todo_id, key, token, this);
  }, function (err, data) {
    if (err) {
      return callback(err);
    }

    for (var i = 0, li = data.cards.length; i < li; i++) {
      todoCardSum += countExtractor(data.cards[i].name);
    }
    
    todoListCount = countExtractor(data.name);
    modifyName = 'To Do (' + todoListCount + ')';

    if (todoListCount !== todoCardSum) { //두 값이 다르면 todoCardSum을 list 제목에 넣도록 수정.
      setListName(todo_id, modifyName, key, token, this);
    } else {
      return;
    }
  }, function (err, data) {
    if (err) {
      return callback(err);
    }

    store.setTotalCount(board_id, todoCardSum, this);
  }, function (err, data) {
    if (err) {
      return callback(err);
    }

    return callback(null, {
      todo_count : todoCardSum
    });
  }
      );

}

function getCardCount(card_id, key, token, callback) {

}

//ToDO와 DOing을 다 get, get하고 이를 토대로 Total을 가져옴ㅋ
//구현해라.
function getDoneCount(done_id, key, token, callback) {
  request({
    method : "GET",
    url : "https://api.trello.com/1/lists/" + done_id,
    qs  : {
      key : key,
      token : token,
      fields : "name",
      cards : "open"
    }
  }, function (err, res, body) {
    if (err) {
      return callback(err);
    }

    console.log('hh', done_id, key, token);
    var count = 0;
    var data = JSON.parse(body);
    
    //Done list내의 카드들에서 카운트를 가져와 count 변수에 다 저장해서 돌려줍니다.
    for (var i = 0, li = data.cards.length; i < li; i++) {
      var t = countExtractor(data.cards[i].name);

      if (t === 0) {
        console.log('Count가 없네요. Count를 추가해주어야 합니다.');
      } else {
        count += t;
      }
    }

    callback(null, {
      done_count : count
    });
  });
}

//Doing list의 제목에서 현재 남은 카운트를 갖고 옵니다.
function getDoingCount(list_id, key, token, callback) {
  step(
      function () {
        getCardBundle(list_id, key, token, this);
      },
      function (err, data) {
        if (err) {
          return callback(err);
        }

        var count = 0;

        //Doing list내의 카드들에서 카운트를 가져와 count 변수에 다 저장해서 돌려줍니다.
        for (var i = 0, li = data.cards.length; i < li; i++) {
          var t = countExtractor(data.cards[i].name);

          if (t === 0) {
            console.log('Count가 없네요. Count를 추가해주어야 합니다.');
          } else {
            count += t;
          }
        }

        callback(null, {
          doing_count : count
        });
      }
      );
}



function getRestCount(board_id, todo_id, doing_id, key, token, callback) {
  var today_count = 0;

  step(function () {
    getTodoCount(board_id, todo_id, key, token, this);
  }, function (err, data) {
    if (err) {
      return callback(err);
    }

    today_count += parseInt(data.todo_count, 10);
    getDoingCount(doing_id, key, token, this);
  }, function (err, data) {
    if (err) {
      return callback(err);
    }

    today_count += parseInt(data.doing_count, 10);

    //현재까지 남은 Doing Count 저장합니다. set할때 Doing list제목에 있는 count도 수정해 주어야함.
    store.setCurrentCount(board_id, today_count, this); 
  }, function (err, data) {
    if (err) {
      return callback(err);
    }

    store.getDailyCount(board_id, this);
  }, function (err, data) {
    if (err) {
      return callback(err);
    }

    return callback(null, {
      today_count : today_count,
      continue_count : data.count.dateCount
    });
  }
  );

}


module.exports.getBoards = getBoard;
module.exports.getList = getList;
module.exports.getCardBundle = getCardBundle;
module.exports.getDoneCount = getDoneCount;
module.exports.getDoingCount = getDoingCount;
module.exports.getTodoCount = getTodoCount;
module.exports.getRestCount = getRestCount;
module.exports.listIdExtractor = listIdExtractor;

"use strict";

var fs = require('fs');
var step = require('step');
var current_path = './current_count.txt';
var total_path = './total_count.txt';
var dateformat = require('dateformat');


//배열의 board_id의 카운트를 찾아준다.
function searchBoardCount(board_id, data, callback) {
  var count_arr = data.split(':');
  var t;

  if (count_arr.length === 3) { // current Count 
    var today = dateformat(new Date(), 'yyyy-mm-dd') + '';
    var day_record = data.split('\n');
    var day_count;

    for (var i = 0, li = day_record.length; i < (li - 1); i++) { //마지막에 \n 들어가니까.
      t = day_record[i].split(':');
      if (t[1] === today && t[0] === board_id) {
        day_count = t[2];
        break;
      }
    }

    return callback(null, {
      count : day_count
    });
  } else if (count_arr.length === 2) { // total Count
    var board_record = data.split('\n');
    var total_count;

    for (var i = 0, li = data.length; i < li; i++) {
      t = board_record.split(':');

      if (t === board_id) {
        total_count = t[1];
        break;
      }
    }

    return callback(null, {
      count : total_count
    });
  } else {
    return callback("ERRRR: CANT GET COUNT");
  }
}

function setCurrentCount(board_id, count, callback) {
  if (!count) {
    return callback("ERROR : count not exists");
  }

  var today = dateformat(new Date(), 'yyyy-mm-dd');
  var record = board_id + ':' + today + ':' + count + '\n';

  step(function () {
    fs.exists(current_path, this);
  }, function (exists) { //없으면 만들어 임마. 현재 카운트 저장은 해야지.
    if (!exists) {
      fs.writeFile(current_path, '', 'utf8');
      return;
    }

    fs.readFile(current_path, 'utf8', this); 
  }, function (err, data) {
    if (err) {
      return callback(err);
    }

    if (data.match(record) !== null) {
      return callback(null);
    }

    fs.appendFile(current_path, record, 'utf8', this);
  }, function (err) {
    if (err) {
      return callback(err);
    }
  
    return callback(null);
  }
      );
}


function getCurrentCount(board_id, callback) {
  var today = dateformat(new Date(), 'yyyy-mm-dd');

  step(function () {
    fs.readFile(current_path, 'utf8', this);
  },
  function (err, data) {
    if (err) {
      return callback(err);
    }
  
    setCurrentCount(board_id, data, this);
  },
  function (err, data) {
    if (err) {
      return callback(err);
    }

    callback(null, {
      current_count : data.count
    });
  }
      );
}

function getDailyCount(board_id, callback) {
  fs.readFile(current_path, 'utf8', function (err, data) {
    if (err) {
      return callback(err);
    }

    var daily_count = [];
    var date = [];
    var dateCount = [];
    var index = 0;
    var data_arr = data.split('\n');
    
    for (var i = 0, li = data_arr.length; i < li; i++) {
      if (data_arr[i].match(board_id) !== null) {
        var t = data_arr[i].split(':');
        dateCount[index] = [t[1], t[2]];
        date[index] = t[1];
        daily_count[index++] = t[2];
      }
    }

    callback(null, {
      count : {
        dateCount : dateCount,
        date : date,
        daily_count : daily_count
      }
    });
    //TODO data는 줄마다 배열로 온다. readFile
  });
}

function setTotalCount(board_id, count, callback) {
  fs.writeFile(total_path, board_id + ':' + count + '\n', 'utf8', function (err) {
    if (err) {
      return callback(err);
    }

    return callback(null);
  });

}

function getTotalCount(board_id, count, callback) {
  step(function () {
    fs.readFile(total_path, 'utf8', this);
  },
  function (err, data) {
    if (err) {
      return callback(err);
    }

    searchBoardCount(board_id, data, this);
  },
  function (err, data) {
    if (err) {
      return callback(err);
    }

    callback(null, {
      total_count : data.count
    });
  }

      );
}

module.exports.setCurrentCount = setCurrentCount;
module.exports.getCurrentCount = getCurrentCount;
module.exports.getDailyCount = getDailyCount;
module.exports.setTotalCount = setTotalCount;
module.exports.getTotalCount = getTotalCount;

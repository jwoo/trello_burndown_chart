
/**
 * Module dependencies.
 */

"use strict";

var express = require('express');
var http = require('http');
var path = require('path');

var app = express();
GLOBAL.settings = require('./config.json');

app.configure(function () {
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'html');

  app.engine('.html', require('ejs-locals'));

  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function () {
  app.use(express.errorHandler());
});

require('./routes/index.js').routes(app);

http.createServer(app).listen(app.get('port'), function () {
  console.log("Express server listening on port " + app.get('port'));
});
